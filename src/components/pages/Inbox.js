import React from 'react'
import TaskView from '../TaskView'
import axios from 'axios';
import { withRouter } from 'react-router-dom';

class Inbox extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            projects: [],
            sections: [],
            labels: [],
            tasks: [],
            view: 'list'
        }
    }
    static getDerivedStateFromProps(nextProps, prevState){
        return {
            ...prevState,
            projects: nextProps.projects,
            sections: nextProps.sections,
            labels: nextProps.labels,
            theme: nextProps.theme,
            tasks: nextProps.tasks
        }
    }
    
    handleCompleteTask = (id) =>{
        console.log(this.props)
        let not_isCompleted = this.state.tasks.filter(task=>{
            return task._id === id
        })
        console.log(not_isCompleted[0].completed)
        console.log('here')
        const token = localStorage.getItem('token');
        const username = localStorage.getItem('username');
        const apiUrl = 'https://todoist-clone-back.herokuapp.com/';
        const body = {
            "username": username,
            "completed": !not_isCompleted[0].completed
        }
        console.log(body.completed);
        fetch(`${apiUrl}tasks/${username}/${id}`,{
            method: "PATCH",
            headers: {"Content-type": "application/json", charset:"UTF-8", "token": token},
            body: JSON.stringify(body)
        }).then(res=> {
            const data = res.json();
            console.log("from response")
            console.log(data)
            return data;
        })
        .then(data=>{
            
            console.log('data is here')
            console.log(data)
            this.setState(prevState=>({
                tasks: prevState.tasks.map(task=>{
                    return task._id===data._id ? {...task, completed: data.completed} : task
                })
            }))
        })
        .catch(err=>console.log(err));

        console.log(this.state.tasks[0])
    }
    changeView = () =>{
        this.setState(prevState=>({
            ...prevState,
            view: prevState.view === 'list' ? 'board' : 'list'
        }))
    }

    render() {
        const { sections, view, tasks } = this.state;
        const theme = this.props.theme;
        return (
            <div className={`w-4/5 flex flex-center flex-col px-10 ${theme==="light"? "" : "bg-gray-700 text-white"}`}>
                <div className="ml-4 mt-10"><h1>INBOX</h1></div>
                <TaskView handleCompleteTask={this.handleCompleteTask} theme={theme} changeView={this.changeView} sections={sections} tasks={tasks} view={view}/>
            </div>
        )
    }
}

export default withRouter(Inbox)
