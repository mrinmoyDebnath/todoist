import React from 'react'
import TaskView from '../TaskView'
import axios from 'axios';

class Today extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            projects: [],
            sections: [],
            labels: [],
            tasks: [],
            view: 'list'
        }
    }
    static getDerivedStateFromProps(nextProps, prevState){
        console.log(nextProps)
        return {
            ...prevState,
            projects: nextProps.projects,
            sections: nextProps.sections,
            labels: nextProps.labels,
            theme: nextProps.theme,
            tasks: nextProps.tasks
        }
    }
    
    handleCompleteTask = (id) =>{
        console.log(id)
    }
    changeView = () =>{
        this.setState(prevState=>({
            ...prevState,
            view: prevState.view === 'list' ? 'board' : 'list'
        }))
    }

    render() {
        console.log(this.state)
        const { sections, view, tasks } = this.state;
        const today = new Date();
        const todayTasks = tasks.filter(task=>{
            const taskDate = new Date(task.task_due_date);
            return today.getDate()===taskDate.getDate() && today.getMonth()===taskDate.getMonth() && today.getFullYear() === taskDate.getFullYear()
        })
        const todaySection = sections.filter(section=>todayTasks.map(task=>task.section_id).includes(section._id))
        const theme = this.props.theme;
        return (
            <div className={`w-4/5 flex flex-center flex-col px-10 ${theme==="light"? "" : "bg-gray-700 text-white"}`}>
                <div className="ml-4 mt-10"><h1>Today {today.getDate()} - {today.getMonth() + 1} - {today.getFullYear()}</h1></div>
                <TaskView handleCompleteTask={this.handleCompleteTask} theme={theme} changeView={this.changeView} sections={todaySection} tasks={todayTasks} view={view}/>
            </div>
        )
    }
}

export default Today
