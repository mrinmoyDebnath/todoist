import React, { Component } from 'react'
import SettingsHeader from '../parts/SettingsHeader'
import SettingsNavbar from '../parts/SettingsNavbar'
import { Route } from 'react-router-dom';
import AccountSettingsView from './AccountSettingsView';
import NotificationSettingsView from './NotificationSettingsView';

class Settings extends Component {
    render() {
        return (
            <>
                <div>
                    <SettingsHeader />
                    <div className="h-full pt-4" style={{maxWidth: "60rem", margin: 'auto'}}>
                        <div className="flex justify-between">
                            <SettingsNavbar />
                            <div className="w-3/4">
                                <Route path="/settings/account" component={AccountSettingsView} />
                                <Route path="/settings/notifications" component={NotificationSettingsView} />
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Settings
