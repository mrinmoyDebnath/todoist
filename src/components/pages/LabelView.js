import React from 'react'
import TaskView from '../TaskView'
import { withRouter } from 'react-router-dom';

class LabelView extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            projects: [],
            labels: [],
            tasks: [],
            sections: [],
            view: 'list'
        }
    }
    static getDerivedStateFromProps(nextProps, prevState){
        console.log(nextProps)
		return {
			...prevState,
			projects: nextProps.projects,
			sections: nextProps.sections
		}
    }
    handleCompleteTask = (id) =>{
        console.log(id)
    }
    changeView = () =>{
        this.setState(prevState=>({
            ...prevState,
            view: prevState.view === 'list' ? 'board' : 'list'
        }))
    }
    // componentDidMount(){
    //     this.setState({
    //         ...this.state,
    //         currentProject: 
    //     })
    // }
    render() {
        const { sections, view, tasks, projects, labels } = this.state;
        const currentProject = labels.filter(label=>this.props.match.params.labelName===label.label_name)
        const filteredSection = currentProject.length > 0? sections.filter(section=>section.project_id===currentProject[0]._id) : []
        const theme = this.props.theme;
        return (
            <div className={`w-4/5 flex flex-center flex-col px-10 ${theme==="light"? "" : "bg-gray-700 text-white"}`}>
                <div className="ml-4 mt-10"><h1>{this.props.match.params.projectName}</h1></div>
                <TaskView handleCompleteTask={this.handleCompleteTask} theme={theme} changeView={this.changeView} sections={filteredSection} tasks={tasks} view={view}/>
            </div>
        )
    }
}

export default withRouter(LabelView)
