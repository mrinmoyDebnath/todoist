import React from 'react'

class AccountSettingsView extends React.Component{
    constructor(props) {
        super(props)
    
        this.state = {
            userName: 'mrinmoy',
            userEmail: 'mrinmoy@gmail.com',
            password: '890--8',
            editName: false,
            editEmail: false,
            editPassword: false
        }
        this.userRef = React.createRef();
        this.emailRef = React.createRef();
        this.passwordRef = React.createRef();

    }
    
    editName = () => {
        this.setState({
            ...this.state,
            editName: true
        }, ()=>this.userRef.current.focus())
    }
    editEmail = () =>{
        this.setState({
            ...this.state,
            editEmail: true,
        }, ()=>this.emailRef.current.focus())
    }
    editPassword = () =>{
        this.setState({
            ...this.state,
            editPassword: true
        }, ()=>this.passwordRef.current.focus())
    }
    saveUserName = e =>{
        if(e.key==='Enter'){
            this.setState({
                ...this.state,
                editName: false,
                userName: e.target.value || this.state.userName
            })
        }
    }
    saveEmail = e =>{
        if(e.key==='Enter'){
            this.setState({
                ...this.state,
                editEmail: false,
                userEmail: e.target.value || this.state.userEmail
            })
        }
    }
    savePassword = e =>{
        if(e.key==='Enter'){
            this.setState({
                ...this.state,
                editEmail: false,
                password: e.target.value || this.state.password
            })
        }
    }
    render(){
        const { userName, userEmail, editName, editEmail, editPassword } = this.state;
        return (
            <div>
                <h4 className="text-lg font-bold">Personal Information</h4>
                <div>
                    <div className="flex justify-between py-2 text-lg">
                        <div className="">Name: </div>
                       {
                            editName
                            ? <input ref={this.userRef}className="bg-white rounded shadow" type="text" onBlur={()=>this.setState({...this.state, editName: false})} onKeyPress={this.saveUserName}/>
                            : <div>{userName}</div>
                        }    
                        <div className="rounded px-4 py-0.5"/>
                     </div>
                    <div className="flex justify-between py-2 text-lg">
                        <div className="">Email: </div>
                        {
                            editEmail
                            ? <input ref={this.emailRef} className="bg-white rounded shadow" type="email" onBlur={()=>this.setState({...this.state, editEmail: false})} onKeyPress={this.saveEmail}/>
                            : <div>{userEmail}</div>
                        }
                        <button className="bg-white shadow rounded px-4 py-0.5" onClick={this.editEmail}>Edit</button>
                    </div>
                    <div className="flex justify-between py-2 text-lg">
                        <div className="">Password: </div>
                        {
                            editPassword
                            ? <input ref={this.passwordRef} className="bg-white rounded shadow" type="password" onBlur={()=>this.setState({...this.state, editPassword: false})} onKeyPress={this.savePassword} />
                            : <div>********</div>
                        }
                        <button className="bg-white shadow rounded px-4 py-0.5" onClick={this.editPassword}>Edit</button>
                    </div>
                </div>
            </div>
        )   
    }
}

export default AccountSettingsView
