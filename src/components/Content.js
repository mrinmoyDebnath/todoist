import React, { Component } from 'react'
import './Content.css';
import { Link } from 'react-router-dom';

class Content extends Component {
    render() {
        return (
            <div>
            <section className="content">
                <div className="heading-section">
                    <h1 className="heading">
                        Organize it all with Todoist
                    </h1>
                    <Link to="/signup" className="get-started">Get Started</Link>
                    
                </div>
            </section>
            <img style={{width:"100%", height:"85%", zIndex:"1"}} srcSet="https://todoist.com/_next/static/images/screenshot@2x_44c1cf78bc12457546d889573e04345a.webp" />
            <img style={{width:"40%", height:"80%", zIndex:"999", marginTop:"-64%", marginLeft:"47%"}} srcSet="https://todoist.com/_next/static/images/screenshot-phone@2x_e35aacb2dec92f091fce33a2ba8e99b5.webp" />
            </div>
        )
    }
}

export default Content;
