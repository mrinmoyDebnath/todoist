import React from 'react'
import { useState } from "react";
import TaskModal from './TaskModal';

let completed = false;
function handleCompleteTask(){
    console.log(completed)
    completed = !completed;
}

function Board(props) {
    const [openModal, setModal] = useState(false)
    const [currentTask, setCurrentTask] = useState(null)
    const onOpenModal = (e,task) =>{
        e.preventDefault()
        setModal(true)
        setCurrentTask(task)
    }

    const onCloseModal = ()=>{
        setModal(false)
    }
    const section = props.section;
    const tasks = props.tasks;
    const theme = props.theme;
    const sectionTasks = tasks.filter(task=>task.section_id===section._id);
    return (
        <li className="py-4 roundedmy-2">

            <TaskModal openModal={openModal} closeModal={onCloseModal} task={currentTask}/>
            <div className="flex items-center justify-center px-4 my-2">
                    <div className='hover:rounded-lg hover:shadow p-2 w-72'>
                        <h3 className={`font-semibold text-lg ${theme==="light"? "text-gray-700": "text-white"} tracking-wide`}>{section.section_name}</h3>
                        <div className="flex justify-start p-1" >
                            <ul className={`${theme==="light"? "text-gray-500": "text-white"} my-1 w-full`}>
                                {
                                    sectionTasks.map(task=>
                                        <li key={task._id} className="p-0.5 w-full relative">
                                            <div className={`flex flex-start p-2 mb-1 ${theme==="light"? "hover:bg-gray-100": "text-white"} hover:shadow  cursor-pointer`} style={{border: '1px solid #e5e7eb', borderRadius: '4px'}}>
                                                <svg xmlns="http://www.w3.org/2000/svg" className={`h-6 w-6 hover:bg-green-300 rounded-full ${completed? 'stroke-2': ''}`} onClick={handleCompleteTask} strokeWidth={0.4} fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path strokeLinecap="round" strokeLinejoin="round" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                </svg>    
                                                <span onClick={e=>onOpenModal(e, task)} style={{lineHeight: '1.5rem'}}>{task.task_name}
                                                </span>
                                            </div>
                                            <svg onClick={()=>props.remove(task._id)} xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 absolute r-0 hover:text-red-300" style={{top: "0.7rem", right: "1rem"}} fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                            </svg>
                                        </li>
                                    )
                                }
                                <li>
                                    <div className="mt-1 py-0.5 px-2 w-full hover:text-red-700 cursor-pointer">
                                        Add task
                                    </div>
                                </li>
                            </ul>
                        </div>
                        {props.children ? props.children : null}
                    </div>
                </div>
        </li>
    )
}

export default Board
