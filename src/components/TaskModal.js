import axios from 'axios';
import React, { Component } from 'react';
import { Modal } from 'react-responsive-modal';
import 'react-responsive-modal/styles.css';
import './parts/Modal.css';

class TaskModal extends Component {
    constructor(props) {
        super(props)

        this.state = {
            view: 'comments',
            taskName: '',
            taskId: null,
            task: null,
            messages: [],
            newMessage: ''
        }
    }
    messageHandler = (e) =>{
        this.setState({
            ...this.state,
            newMessage: e.target.value
        })
    }
    changeView = (viewType) => {
        const view = this.state.view;
        if (view === 'comments' && view != viewType) {
            this.setState({
                view: viewType
            })
        }
        else if (view === 'task' && view != viewType) {
            this.setState({
                view: viewType
            })
        }
    }
    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            ...prevState,
            taskName: nextProps.task ? nextProps.task.task_name : '',
            task: nextProps.task ? nextProps.task : null,
            taskId: nextProps.task ? nextProps.task._id : null,
        }
    }
    getMessages = () => {
        const token = localStorage.getItem('token');
        const username = localStorage.getItem('username');
        const apiUrl = 'https://todoist-clone-back.herokuapp.com/';
        axios({
            method: 'get',
            url: apiUrl + `comments/${this.state.taskId}`,
            headers: { "token": token },
        })
            .then(res => {
                this.setState({
                    ...this.state,
                    messages: res.data
                })
            })
            .catch(err => console.log(err))
    }
    addMessage = ()=>{
        const token = localStorage.getItem('token');
        const username = localStorage.getItem('username');
        const apiUrl = 'https://todoist-clone-back.herokuapp.com/';
        const message = this.state.newMessage;
        console.log(this.state)
        axios({
            method: 'post',
            headers: {"token": token},
            url: apiUrl+`comments/comment`,
            body:{
                "username" : username,
                "comment_text": message,
                "task_id": this.state.taskId
            }
        })
        .then(res=>console.log(res.data))
        .catch(err=>console.log(err))
        .finally(()=>this.getMessages())
    }
    componentDidMount() {
        this.getMessages();
    }
    render() {

        const { view, messages } = this.state;
        return (
            <Modal open={this.props.openModal} onClose={this.props.closeModal}
                classNames={{
                    overlay: 'customOverlay',
                    modal: 'customModal',
                }}
            >
                <div>
                    <div className="p-1 flex justify-center w-32 rounded" style={{ border: "1px solid #D3D3D3" }}>
                        <svg width="16" height="16" viewBox="0 0 16 16"><g fill="currentColor"><path d="M13.5 9.5V12a1.5 1.5 0 01-1.5 1.5H4A1.5 1.5 0 012.5 12V9.5h3.75a1.75 1.75 0 003.5 0h3.75z" opacity="0.1"></path><path d="M10.491 2a2 2 0 011.923 1.45l1.509 5.28a2 2 0 01.077.55V12a2 2 0 01-2 2H4a2 2 0 01-2-2V9.28a2 2 0 01.077-.55l1.509-5.28A2 2 0 015.509 2h4.982zm0 1H5.51a1 1 0 00-.962.725l-1.509 5.28A1 1 0 003 9.28V12a1 1 0 001 1h8a1 1 0 001-1V9.28a1 1 0 00-.038-.275l-1.51-5.28a1 1 0 00-.96-.725zM6.25 9a.5.5 0 01.5.5 1.25 1.25 0 002.5 0 .5.5 0 01.5-.5h1.75a.5.5 0 110 1h-1.306a2.25 2.25 0 01-4.388 0H4.5a.5.5 0 010-1z"></path></g></svg>
                        <span className="text-gray-400 text-xs ml-1">{this.state.taskName}</span>
                    </div>
                    <div className="flex justify-between mt-2">
                        <div className={`w-1/2 text-center ${view === 'comments' ? "font-bold" : ''}`} style={{ borderBottom: `${view === 'comments' ? "1px solid black" : '1px solid #e0e0e0'}` }}><span onClick={() => this.changeView('comments')} className="cursor-pointer">Messages</span></div>
                        <div className={`w-1/2 text-center ${view === 'comments' ? "" : 'font-bold'}`} style={{ borderBottom: `${view === 'comments' ? "1px solid #e0e0e0" : '1px solid black'}` }}><span onClick={() => this.changeView('task')} className="cursor-pointer">Task Description</span></div>
                    </div>
                    <div className="comments-area">
                        {
                            view === 'comments'
                                ? messages.length > 0
                                    ?<> <ul>
                                        {
                                            messages.map((message, index) => <li key={index}>{message}</li>)
                                        }
                                        </ul>
                                        <div className="flex flex-col p-1" style={{ width: 'max-content' }}>
                                            <div className="rounded-lg" style={{ border: "1px solid #D3D3D3" }}>
                                                <input value={this.state.newMessage} onChange={this.messageHandler} placeholder="e.g. Meeting" type="text" className="mb-2 p-2 focus:outline-none" />
                                            </div>
                                            <button onClick={this.addMessage()} className="w-1/2 text-center mt-5 bg-red-600 px-3.5 py-2 rounded-lg font-bold text-white hover:bg-red-400">Add task</button>
                                        </div>
                                    </>
                                    : <div style={{ margin: 'auto' }} className="w-1/2">
                                        <svg dataSvgsPath="theme_todoist/project_comments.svg" xmlns="http://www.w3.org/2000/svg" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="1.5" clipRule="evenodd" viewBox="0 0 220 200"><g transform="translate(-2800 -740)"><g id="ProjectComments"><path fill="none" d="M1800-100h300v300h-300z" transform="matrix(.73333 0 0 .66667 1480 806.667)"></path><g transform="translate(-300.283 -876.614)"><g id="ProjectCommentsA"><path fill="#f9e4e2" stroke="#f9e4e2" strokeWidth="2.001" d="M3142.316 1760.895h13.136v12.534h-13.136z"></path><path fill="none" stroke="#fff" strokeLinecap="butt" strokeLinejoin="miter" strokeWidth="2" d="M3144.95 1766.843l2.58 2.921 5.295-5.2"></path><path fill="#f9e4e2" stroke="#f9e4e2" strokeWidth="2.001" d="M3142.316 1778.181h13.136v12.534h-13.136z"></path><path fill="none" stroke="#fff" strokeLinecap="butt" strokeLinejoin="miter" strokeWidth="2" d="M3144.95 1784.123l2.58 2.921 5.295-5.2"></path><path fill="#f9e4e2" stroke="#f9e4e2" strokeWidth="2.001" d="M3142.316 1795.466h13.136V1808h-13.136z"></path><path fill="none" stroke="#fff" strokeLinecap="butt" strokeLinejoin="miter" strokeWidth="2" d="M3144.95 1801.413l2.58 2.921 5.295-5.2"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.94" d="M16637 1390h110" transform="matrix(.51751 0 0 .80957 -5445.68 638.025)"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.94" d="M16637 1390h110" transform="matrix(.51751 0 0 .80957 -5445.68 656.074)"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.94" d="M16637 1390h110" transform="matrix(.51751 0 0 .80957 -5445.68 674.808)"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.94" d="M16637 1390h110" transform="matrix(.51751 0 0 .80957 -5445.68 643.813)"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.94" d="M16637 1390h110" transform="matrix(.51751 0 0 .80957 -5445.68 661.862)"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.94" d="M16637 1390h110" transform="matrix(.51751 0 0 .80957 -5445.68 680.596)"></path><path fill="#eca19a" d="M3270 3474c0-16.56-8.69-30-19.4-30h-141.2c-10.71 0-19.4 13.44-19.4 30 0 16.56 8.69 30 19.4 30h141.2c10.71 0 19.4-13.44 19.4-30z" transform="matrix(.698 0 0 .45134 956.743 169.149)"></path><path fill="#fff" d="M3225.36 3550.5c-8.43 1.96-3.82 15.61.35 19.66 6.22 6.03 18.45 8.64 26.82 6.83 5.51-1.19 10.29-5.03 8.61-11.19-1.17-4.28-4.03-7.94-6.64-11.42" transform="translate(245.992 -1561.34) scale(.92336)"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.4" d="M2920 3494h-40c1.07 7.5 1.78 24.21 0 35h20l5 10 5-10h10c2.09-10.98 1.83-22.74 0-35z" transform="matrix(.87936 0 -.0977 .87936 929.25 -1410.86)"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.12" d="M15739.5 990.429c7 1.038 14.3.248 21.4.334" transform="matrix(1.04218 -.02832 .0226 .83143 -13299.4 1297.18)"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.12" d="M15739.5 990.429c7 1.038 14.3.248 21.4.334" transform="matrix(1.04218 -.02832 .0226 .83143 -13299.5 1302.34)"></path><path fill="#f9e4e2" d="M2920 3494h-40c1.07 7.5 2.81 26.35 0 35h20l1.87 8.97 6.24-8.97H2920c2.09-10.98 1.83-22.74 0-35z" transform="matrix(1.09377 0 -.12153 1.09377 446.851 -2197.4)"></path><path fill="#fae5e3" stroke="#fff" strokeWidth="2" d="M15739.5 990.429c7 1.038 14.3.248 21.4.334" transform="matrix(1.15099 -.0335 .02044 .83153 -14954.5 1343.6)"></path><path fill="#fae5e3" stroke="#fff" strokeWidth="2" d="M15739.5 990.429c7 1.038 14.3.248 21.4.334" transform="matrix(1.15121 -.02171 .01005 .83178 -14947.4 1162.76)"></path><path fill="none" stroke="#da4c3f" strokeWidth="1.999" d="M3249.306 1718.98c-8.661-11.289-12.756-20.208-22.586-18.293-3.706.718-6.89 2.948-10.004 4.973-5.797 3.754-9.87 6.637-16.072 9.785"></path><path fill="none" stroke="#da4c3f" strokeWidth="1.999" d="M3220.052 1715.849c2.225-1.224 5.321-3.142 7.947-3.053 6.094.205 11.629 14.396 15.864 20.838"></path><g transform="matrix(.565 -.0122 -.0482 .53274 1787.66 -180.946)"><path fill="#fff" d="M2843.93 3578.3c-1.2-7.46-2.67-15.03-4.28-22.72l-93.39-1.58c1.74 34.73-11.28 61.21-6.26 100l24.83.29 5.79 24.06 14.67-22.24L2840 3654c4.69-17.12 6.24-32.97 6.26-51.88 0-2.25-1.59-19.16-2.33-23.82z"></path><clipPath id="_clip1"><path d="M2843.93 3578.3c-1.2-7.46-2.67-15.03-4.28-22.72l-93.39-1.58c1.74 34.73-11.28 61.21-6.26 100l24.83.29 5.79 24.06 14.67-22.24L2840 3654c4.69-17.12 6.24-32.97 6.26-51.88 0-2.25-1.59-19.16-2.33-23.82z"></path></clipPath><g clipPath="url(#_clip1)"><path fill="#f9e4e2" d="M2852.69 3588.68c25.27-2.68 21.57 23.92 53.25 25.92l-1.07 5.13-10.87 24.17c-11.89-1.5-22.65-2.15-22.65-2.15s-7.33-23.62-33.81-25.27l-10.53-10.13" transform="matrix(1.63217 -.2528 .4754 1.97699 -3615.82 -2818.34)"></path></g><path fill="none" stroke="#eca19a" strokeWidth="3.86" d="M2843.93 3578.3c-1.2-7.46-2.67-15.03-4.28-22.72l-93.39-1.58c1.74 34.73-11.28 61.21-6.26 100l24.83.29 5.79 24.06 14.67-22.24L2840 3654c4.69-17.12 6.24-32.97 6.26-51.88 0-2.25-1.59-19.16-2.33-23.82z"></path></g><path fill="none" stroke="#da4c3f" strokeWidth="3.86" d="M2738.85 3637.3c.05 5.34.4 10.88 1.15 16.7l24.83.29 5.79 24.06 14.67-22.24L2840 3654c4.69-17.12 6.24-32.97 6.26-51.88 0-.42-.06-1.36-.15-2.63" transform="matrix(.565 -.0122 -.0482 .53274 1787.66 -180.946)"></path><path fill="none" stroke="#e57f78" strokeWidth="1.61" d="M15739.5 990.429c7 1.038 14.3.248 21.4.334" transform="matrix(1.4758 -.04136 .02793 .94664 -20079 1416.74)"></path><path fill="none" stroke="#e57f78" strokeWidth="1.61" d="M15739.5 990.429c7 1.038 14.3.248 21.4.334" transform="matrix(1.4758 -.04136 .02793 .94664 -20079.1 1422.61)"></path><path fill="#fff" stroke="#da4c3f" strokeWidth="1.999" d="M3242.967 1732.167c-2.173 1.643-5.637 1.658-8.139 1.05-7.542-1.834-13.424-7.358-20.447-10.326-7.031-2.967-12.759-1.224-8.54 2.95 7.292 7.237 34.48 29.065 52.386 36.913 8.04 3.524 32.083 14.373 32.083 14.373"></path><path fill="none" stroke="#da4c3f" strokeWidth="1.999" d="M3241.193 1729.055c.94 1.721 2.669 4.727 3.509 6.003M3241.09 1702.915c5.76-1.009 8.577 2.808 11.25 6.934 1.577 2.433 4.672 7.532 6.03 9.768M3251.222 1703.28c2.386.133 5.175 1.794 6.541 3.723.824 1.165 5.374 9.723 7.976 14.035M3261.261 1712.706c7.618-.876 12.828 14.52 16.513 21.272 6.878 12.597 15.062 19.207 28.054 22.313M3231.041 1714.449c-2.287 4.491-2.453 9.819-3.789 14.624"></path><path fill="none" stroke="#da4c3f" strokeWidth="2.9" d="M2920.23 3609.25c-1.75 3.41-2.91 7.1-4.83 10.41" transform="matrix(.62629 -.10362 .12056 .72864 972.388 -606.689)"></path><circle cx="3025" cy="3579" r="15" fill="#fff" transform="matrix(.45134 0 0 .45134 1762.83 121.759)"></circle></g></g></g></g></svg>
                                        <div className="flex flex-col p-1" style={{ width: 'max-content' }}>
                                            <div className="rounded-lg" style={{ border: "1px solid #D3D3D3" }}>
                                                <input value={this.state.newMessage} onChange={this.messageHandler} placeholder="e.g. Meeting" type="text" className="mb-2 p-2 focus:outline-none" />
                                            </div>
                                            <button onClick={this.addMessage} className="w-1/2 text-center mt-5 bg-red-600 px-3.5 py-2 rounded-lg font-bold text-white hover:bg-red-400">Add task</button>
                                        </div>
                                    </div>
                                :
                                this.state.task.description
                                    ? <ul>
                                        {
                                            <h1>{this.state.task.description}</h1>
                                        }
                                    </ul>
                                    : <div style={{ margin: 'auto' }} className="w-1/2">
                                        <svg dataSvgsPath="theme_todoist/project_comments.svg" xmlns="http://www.w3.org/2000/svg" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round" strokeMiterlimit="1.5" clipRule="evenodd" viewBox="0 0 220 200"><g transform="translate(-2800 -740)"><g id="ProjectComments"><path fill="none" d="M1800-100h300v300h-300z" transform="matrix(.73333 0 0 .66667 1480 806.667)"></path><g transform="translate(-300.283 -876.614)"><g id="ProjectCommentsA"><path fill="#f9e4e2" stroke="#f9e4e2" strokeWidth="2.001" d="M3142.316 1760.895h13.136v12.534h-13.136z"></path><path fill="none" stroke="#fff" strokeLinecap="butt" strokeLinejoin="miter" strokeWidth="2" d="M3144.95 1766.843l2.58 2.921 5.295-5.2"></path><path fill="#f9e4e2" stroke="#f9e4e2" strokeWidth="2.001" d="M3142.316 1778.181h13.136v12.534h-13.136z"></path><path fill="none" stroke="#fff" strokeLinecap="butt" strokeLinejoin="miter" strokeWidth="2" d="M3144.95 1784.123l2.58 2.921 5.295-5.2"></path><path fill="#f9e4e2" stroke="#f9e4e2" strokeWidth="2.001" d="M3142.316 1795.466h13.136V1808h-13.136z"></path><path fill="none" stroke="#fff" strokeLinecap="butt" strokeLinejoin="miter" strokeWidth="2" d="M3144.95 1801.413l2.58 2.921 5.295-5.2"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.94" d="M16637 1390h110" transform="matrix(.51751 0 0 .80957 -5445.68 638.025)"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.94" d="M16637 1390h110" transform="matrix(.51751 0 0 .80957 -5445.68 656.074)"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.94" d="M16637 1390h110" transform="matrix(.51751 0 0 .80957 -5445.68 674.808)"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.94" d="M16637 1390h110" transform="matrix(.51751 0 0 .80957 -5445.68 643.813)"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.94" d="M16637 1390h110" transform="matrix(.51751 0 0 .80957 -5445.68 661.862)"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.94" d="M16637 1390h110" transform="matrix(.51751 0 0 .80957 -5445.68 680.596)"></path><path fill="#eca19a" d="M3270 3474c0-16.56-8.69-30-19.4-30h-141.2c-10.71 0-19.4 13.44-19.4 30 0 16.56 8.69 30 19.4 30h141.2c10.71 0 19.4-13.44 19.4-30z" transform="matrix(.698 0 0 .45134 956.743 169.149)"></path><path fill="#fff" d="M3225.36 3550.5c-8.43 1.96-3.82 15.61.35 19.66 6.22 6.03 18.45 8.64 26.82 6.83 5.51-1.19 10.29-5.03 8.61-11.19-1.17-4.28-4.03-7.94-6.64-11.42" transform="translate(245.992 -1561.34) scale(.92336)"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.4" d="M2920 3494h-40c1.07 7.5 1.78 24.21 0 35h20l5 10 5-10h10c2.09-10.98 1.83-22.74 0-35z" transform="matrix(.87936 0 -.0977 .87936 929.25 -1410.86)"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.12" d="M15739.5 990.429c7 1.038 14.3.248 21.4.334" transform="matrix(1.04218 -.02832 .0226 .83143 -13299.4 1297.18)"></path><path fill="#fff" stroke="#f9e4e2" strokeWidth="2.12" d="M15739.5 990.429c7 1.038 14.3.248 21.4.334" transform="matrix(1.04218 -.02832 .0226 .83143 -13299.5 1302.34)"></path><path fill="#f9e4e2" d="M2920 3494h-40c1.07 7.5 2.81 26.35 0 35h20l1.87 8.97 6.24-8.97H2920c2.09-10.98 1.83-22.74 0-35z" transform="matrix(1.09377 0 -.12153 1.09377 446.851 -2197.4)"></path><path fill="#fae5e3" stroke="#fff" strokeWidth="2" d="M15739.5 990.429c7 1.038 14.3.248 21.4.334" transform="matrix(1.15099 -.0335 .02044 .83153 -14954.5 1343.6)"></path><path fill="#fae5e3" stroke="#fff" strokeWidth="2" d="M15739.5 990.429c7 1.038 14.3.248 21.4.334" transform="matrix(1.15121 -.02171 .01005 .83178 -14947.4 1162.76)"></path><path fill="none" stroke="#da4c3f" strokeWidth="1.999" d="M3249.306 1718.98c-8.661-11.289-12.756-20.208-22.586-18.293-3.706.718-6.89 2.948-10.004 4.973-5.797 3.754-9.87 6.637-16.072 9.785"></path><path fill="none" stroke="#da4c3f" strokeWidth="1.999" d="M3220.052 1715.849c2.225-1.224 5.321-3.142 7.947-3.053 6.094.205 11.629 14.396 15.864 20.838"></path><g transform="matrix(.565 -.0122 -.0482 .53274 1787.66 -180.946)"><path fill="#fff" d="M2843.93 3578.3c-1.2-7.46-2.67-15.03-4.28-22.72l-93.39-1.58c1.74 34.73-11.28 61.21-6.26 100l24.83.29 5.79 24.06 14.67-22.24L2840 3654c4.69-17.12 6.24-32.97 6.26-51.88 0-2.25-1.59-19.16-2.33-23.82z"></path><clipPath id="_clip1"><path d="M2843.93 3578.3c-1.2-7.46-2.67-15.03-4.28-22.72l-93.39-1.58c1.74 34.73-11.28 61.21-6.26 100l24.83.29 5.79 24.06 14.67-22.24L2840 3654c4.69-17.12 6.24-32.97 6.26-51.88 0-2.25-1.59-19.16-2.33-23.82z"></path></clipPath><g clipPath="url(#_clip1)"><path fill="#f9e4e2" d="M2852.69 3588.68c25.27-2.68 21.57 23.92 53.25 25.92l-1.07 5.13-10.87 24.17c-11.89-1.5-22.65-2.15-22.65-2.15s-7.33-23.62-33.81-25.27l-10.53-10.13" transform="matrix(1.63217 -.2528 .4754 1.97699 -3615.82 -2818.34)"></path></g><path fill="none" stroke="#eca19a" strokeWidth="3.86" d="M2843.93 3578.3c-1.2-7.46-2.67-15.03-4.28-22.72l-93.39-1.58c1.74 34.73-11.28 61.21-6.26 100l24.83.29 5.79 24.06 14.67-22.24L2840 3654c4.69-17.12 6.24-32.97 6.26-51.88 0-2.25-1.59-19.16-2.33-23.82z"></path></g><path fill="none" stroke="#da4c3f" strokeWidth="3.86" d="M2738.85 3637.3c.05 5.34.4 10.88 1.15 16.7l24.83.29 5.79 24.06 14.67-22.24L2840 3654c4.69-17.12 6.24-32.97 6.26-51.88 0-.42-.06-1.36-.15-2.63" transform="matrix(.565 -.0122 -.0482 .53274 1787.66 -180.946)"></path><path fill="none" stroke="#e57f78" strokeWidth="1.61" d="M15739.5 990.429c7 1.038 14.3.248 21.4.334" transform="matrix(1.4758 -.04136 .02793 .94664 -20079 1416.74)"></path><path fill="none" stroke="#e57f78" strokeWidth="1.61" d="M15739.5 990.429c7 1.038 14.3.248 21.4.334" transform="matrix(1.4758 -.04136 .02793 .94664 -20079.1 1422.61)"></path><path fill="#fff" stroke="#da4c3f" strokeWidth="1.999" d="M3242.967 1732.167c-2.173 1.643-5.637 1.658-8.139 1.05-7.542-1.834-13.424-7.358-20.447-10.326-7.031-2.967-12.759-1.224-8.54 2.95 7.292 7.237 34.48 29.065 52.386 36.913 8.04 3.524 32.083 14.373 32.083 14.373"></path><path fill="none" stroke="#da4c3f" strokeWidth="1.999" d="M3241.193 1729.055c.94 1.721 2.669 4.727 3.509 6.003M3241.09 1702.915c5.76-1.009 8.577 2.808 11.25 6.934 1.577 2.433 4.672 7.532 6.03 9.768M3251.222 1703.28c2.386.133 5.175 1.794 6.541 3.723.824 1.165 5.374 9.723 7.976 14.035M3261.261 1712.706c7.618-.876 12.828 14.52 16.513 21.272 6.878 12.597 15.062 19.207 28.054 22.313M3231.041 1714.449c-2.287 4.491-2.453 9.819-3.789 14.624"></path><path fill="none" stroke="#da4c3f" strokeWidth="2.9" d="M2920.23 3609.25c-1.75 3.41-2.91 7.1-4.83 10.41" transform="matrix(.62629 -.10362 .12056 .72864 972.388 -606.689)"></path><circle cx="3025" cy="3579" r="15" fill="#fff" transform="matrix(.45134 0 0 .45134 1762.83 121.759)"></circle></g></g></g></g></svg>
                                    </div>
                        }
                    </div>
                </div>
            </Modal>
        )
    }
}

export default TaskModal
