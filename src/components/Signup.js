import React, { Component } from 'react';
import './Signup.css';
import { Link } from 'react-router-dom';


class Signup extends Component {

    constructor() {
        super();
        this.state = {
          input: {},
          errors: {}
        };
         
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
      
      // Getting value from the form

      handleChange(event) {
        let input = this.state.input;
        input[event.target.name] = event.target.value;
        
        this.setState({
          input
        });
      }
    
      // Handle Sign up data

      handleSubmit(event) {
        event.preventDefault();
        if(this.validate()){
      
            let input = {};
            input["username"] = "";
            input["email"] = "";
            input["password"] = "";
            this.setState({input:input});

            return fetch('https://todoist-clone-back.herokuapp.com/users/signup', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(this.state.input)
            }).then(response => response.json())
              .then(data => console.log(data._id))    
        }
      }

      // Validating a user details in valid format or not 

      validate(){
        let input = this.state.input;
        let errors = {};
        let isValid = true;
     
        if (!input["username"]) {
          isValid = false;
          errors["username"] = "Please enter your username.";
        }
    
        if (typeof input["username"] !== "undefined") {
          const re = /^[a-zA-Z]+([_]?[a-zA-Z0-9])*$/;
          if(input["username"].length < 6 || !re.test(input["username"])){
              isValid = false;
              errors["username"] = "Please enter valid username.";
          }
        }
    
        if (!input["email"]) {
          isValid = false;
          errors["email"] = "Please enter your email Address.";
        }
    
        if (typeof input["email"] !== "undefined") {
            
          var pattern = new RegExp( /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);
          if (!pattern.test(input["email"])) {
            isValid = false;
            errors["email"] = "Please enter valid email address.";
          }
        }
    
        if (!input["password"]) {
          isValid = false;
          errors["password"] = "Please enter your password.";
        }
    
        if (!input["confirm_password"]) {
          isValid = false;
          errors["confirm_password"] = "Please enter your confirm password.";
        }
    
        if (typeof input["password"] !== "undefined") {
          if(input["password"].length < 6){
              isValid = false;
              errors["password"] = "Please add at least 6 charachter.";
          }
        }
    
        if (typeof input["password"] !== "undefined" && typeof input["confirm_password"] !== "undefined") {
            
          if (input["password"] != input["confirm_password"]) {
            isValid = false;
            errors["password"] = "Passwords don't match.";
          }
        }
    
        this.setState({
          errors: errors
        });
    
        return isValid;
    }


    render() {
        return (
            <main className="signup_page">

                <div className="signup_page_frame">
                    <div className="signup_page_logo">
                        <div aria-label="Todoist Homepage">
                            <img src="https://d3ptyyxy2at9ui.cloudfront.net/logo-e7e40b.svg" alt=""/>
                        </div>
                    </div>

                    <div className="signup_page_content">
                        <div id="signup" className="signup_form">
                            <div class="signup_items">
                                <div class="process_one">

                                    <h1>Sign up</h1>

                                    <div className="social_button button_google">
                                        <img width="16" height="16" src="https://d3ptyyxy2at9ui.cloudfront.net/google-41de20.svg" alt=""/>
                                        Continue with Google
                                    </div>
                                    <div className="social_button button_facebook">
                                        <img width="16" height="16" src="https://d3ptyyxy2at9ui.cloudfront.net/facebook-fadd25.svg" alt=""/>
                                        Continue with Facebook
                                    </div>
                                    <div className="social_button button_apple">
                                        <img width="16" height="16" src="https://d3ptyyxy2at9ui.cloudfront.net/apple-728ddf.svg" alt=""/>
                                        Continue with Apple
                                    </div>

                                    <div className="seperator">
                                        <div className="middle_seperator">
                                            <h2><span>OR</span></h2>
                                        </div>
                                    </div>

                                    <form class="email-form" onSubmit={this.handleSubmit}>
                                        <div className="field">
                                            <label className="label" htmlFor="username">Username</label>
                                            <input type="text" name="username" value={this.state.input.username} onChange={this.handleChange} id="username"/>
                                            <div className="isa_error">{this.state.errors.username}</div>
                                        </div>

                                        <div className="field">
                                            <label className="label" htmlFor="email">Email</label>
                                            <input type="email" name="email" value={this.state.input.email} onChange={this.handleChange} id="email"/>
                                            <div className="isa_error">{this.state.errors.email}</div>
                                        </div>

                                        <div className="field">
                                            <label className="label" htmlFor="password">Password</label>
                                            <div className="relative w1 pr-9">
                                                <input type="password" name="password" value={this.state.input.password} onChange={this.handleChange} id="password"/>
                                                <div className="isa_error">{this.state.errors.password}</div>
                                            </div>
                                        </div>

                                        <div className="field">
                                            <label className="label" htmlFor="password">Confirm Password</label>
                                            <div className="relative w1 pr-9">
                                                <input type="password" name="confirm_password" value={this.state.input.confirm_password} onChange={this.handleChange} id="confirm_password" />
                                                <div className="isa_error">{this.state.errors.password}</div>
                                            </div>
                                        </div>

                                        <button className="submit_button submit_button_red" type="submit">
                                            <span>Sign up with Email</span>
                                        </button>
                                    </form>
 
                                    <div class="agreement_text">
                                        By continuing with Google, Apple, or Email, you agree to Todoist's&nbsp;
                                        <span className="text-red-600">Terms of Service</span> and <span className="text-red-600">Privacy Policy</span>
                                    </div>

                                    <div class="login-seperator"></div>

                                    <div className="mt-5 text-center text-xs">
                                        <div className="pb-6">
                                            Already signed up? <Link to="/login" className="text-red-500">Go to login</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </main>
        )
    }
}


export default Signup;