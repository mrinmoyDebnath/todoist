import React, { useState } from 'react'
import Board from './Board';
import List from './List';
import NoTask from './parts/NoTask';
import 'react-responsive-modal/styles.css';
import './parts/Modal.css';
import MessageModal from './parts/MessageModal';
import TaskModal from './TaskModal';

class TaskView extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            openModal: false,
            optionsBlur: true,
            sortBlur: true,
            sectionBlur: true,
            newSection: '',
            openTaskModal: false,
            currentTask: null,
        }
        this.profileModal = React.createRef();
        this.sortModal = React.createRef();
    }
    displayOptions = (e) => {
        this.setState({
            ...this.state,
            optionsBlur: false
        }, () => {
            this.profileModal.current.focus();
        })
    }
    hideOptions = (e) => {
        // console.log(this.profileModal.current)
        this.setState({
            ...this.state,
            optionsBlur: true
        })
    }
    displaySortOptions = (e) => {
        this.setState({
            ...this.state,
            sortBlur: false
        }, () => {
            this.sortModal.current.focus();
        })
    }
    hideSortOptions = (e) => {
        // console.log(this.profileModal.current)
        this.setState({
            ...this.state,
            sortBlur: true
        })
    }
    onClickButton = e => {
        e.preventDefault()
        this.setState({ openModal: true })
    }

    onCloseModal = () => {
        this.setState({ openModal: false })
    }
    onOpenTaskModal = (e, task) => {
        e.preventDefault()
        this.setState({
            openTaskModal: true,
            currentTask:  task
        })
    }

    onCloseTaskModal = () => {
        this.setState({ openTaskModal: false })
    }
    removeSection = (id) =>{
        console.log(id)
    }
    removeTask = (id) =>{
        console.log(id)
    }
    addSection = () =>[
        this.setState({
            ...this.state,
            sectionBlur: false,
        })
    ]
    setNewSection = e =>{
        this.setState({
            ...this.state,
            newSection: e.target.value
        })
    }
    render() {
        const optionsBlur = this.state.optionsBlur;
        const sortBlur = this.state.sortBlur;
        const sectionBlur = this.state.sectionBlur;
        const { sections, view, theme, tasks } = this.props;
        return (
            <>
                <div className='flex justify-start w-full'>
                    <div className="w-full">
                        {

                            sections.length > 0
                                ?
                                <>
                                    <MessageModal openModal={this.state.openModal} closeModal={this.onCloseModal} />
                                    <div className="flex justify-end px-12">
                                        <div className="mx-2" dataLogo="messages" onClick={this.onClickButton}>
                                            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 cursor-pointer" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M8 10h.01M12 10h.01M16 10h.01M9 16H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-5l-5 5v-5z" />
                                            </svg>
                                        </div>
                                        {/* <div className="mx-2 cursor-pointer" dataLogo="sort"  tabIndex="0" onFocus={e => this.displaySortOptions(e)}>
                                            <svg width="24" height="24" viewBox="0 0 24 24"><path fill="currentColor" strokeWidth={2} d="M16.854 5.146l3 3a.502.502 0 01-.708.708L17 6.707V18.5a.5.5 0 01-1 0V6.707l-2.146 2.147a.502.502 0 01-.708-.708l3-3a.502.502 0 01.708 0zM7.5 5a.5.5 0 01.5.5v11.791l2.146-2.145a.502.502 0 01.708.708l-3 3a.502.502 0 01-.708 0l-3-3a.502.502 0 01.708-.708L7 17.293V5.5a.5.5 0 01.5-.5z"></path></svg>
                                        </div> */}
                                        <div className="mx-2" dataLogo="task-view-menu" tabIndex="0" onFocus={e => this.displayOptions(e)}>
                                            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 cursor-pointer" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M8 12h.01M12 12h.01M16 12h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                            </svg>
                                        </div>
                                        {
                                            !optionsBlur
                                                ?   <div className="relative z-10">
                                                        <div tabIndex="0" ref={this.profileModal} onBlur={this.hideOptions} className="w-48 flex flex-col absolute right-0.5 top-6 bg-gray-50 rounded p-2 pl-2 text-gray-700 shadow">
                                                            {
                                                                view==='list'
                                                                ?   <div className="text-sm flex hover:bg-gray-100 cursor-pointer" onClick={this.props.changeView}>
                                                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M9 17V7m0 10a2 2 0 01-2 2H5a2 2 0 01-2-2V7a2 2 0 012-2h2a2 2 0 012 2m0 10a2 2 0 002 2h2a2 2 0 002-2M9 7a2 2 0 012-2h2a2 2 0 012 2m0 10V7m0 10a2 2 0 002 2h2a2 2 0 002-2V7a2 2 0 00-2-2h-2a2 2 0 00-2 2" />
                                                                        </svg>
                                                                        <h4 className="ml-4">View as board</h4>
                                                                    </div>
                                                                :   <div className="text-sm flex hover:bg-gray-100 cursor-pointer" onClick={this.props.changeView}>
                                                                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 10h16M4 14h16M4 18h16" />
                                                                        </svg>
                                                                        <h4 className="ml-4">View as List</h4>
                                                                    </div>
                                                            }
                                                        </div>
                                                    </div>

                                                : null
                                        }
                                        {
                                            !sortBlur
                                                ?   <div className="relative z-10">
                                                        <div tabIndex="0" ref={this.sortModal} onBlur={this.hideSortOptions} className="w-48 flex flex-col absolute right-2 top-6 bg-gray-50 rounded p-2 pl-2 text-gray-700 shadow">
                                                            <div className="mb-2 text-sm flex hover:bg-gray-100 cursor-pointer">
                                                                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
                                                                </svg>
                                                                <h4 className="ml-4">Sort by due date</h4>
                                                            </div>
                                                            <div className="mb-2 text-sm flex hover:bg-gray-100 cursor-pointer" onClick={() => console.log('logout clicked')}>
                                                                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M3 21v-4m0 0V5a2 2 0 012-2h6.5l1 1H21l-3 6 3 6h-8.5l-1-1H5a2 2 0 00-2 2zm9-13.5V9" />
                                                                </svg>
                                                                <h4 className="ml-4">Sort by priority</h4>
                                                            </div>
                                                            <div className="mb-2 text-sm flex hover:bg-gray-100 cursor-pointer" onClick={() => console.log('logout clicked')}>
                                                                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M3 4h13M3 8h9m-9 4h6m4 0l4-4m0 0l4 4m-4-4v12" />
                                                                </svg>
                                                                <h4 className="ml-4">Sort alphabetically</h4>
                                                            </div>
                                                        </div>
                                                    </div>

                                                : null
                                        }
                                    </div>
                                    <TaskModal openModal={this.state.openTaskModal} closeModal={this.onCloseTaskModal} task={this.state.currentTask}/>
                                    <ul className={`flex ${view === 'board' ? 'flex-row flex-wrap' : 'flex-col'}  w-full ${theme==="light"? "text-gray-500": "text-white"} my-1 text-sm mt-4 w-full pl-4 pr-14` }>
                                    {
                                        tasks.filter(tasks=>!sections.map(section=>section._id).includes(tasks.section_id)).map(task=>
                                            <li key={task._id} className={`w-full flex justify-between relative`}>
                                                <div className="flex flex-start p-2 mb-1 cursor-pointer" >
                                                    <svg xmlns="http://www.w3.org/2000/svg" className={`h-6 w-6 hover:bg-green-300 rounded-full mr-4 ${task.completed? "text-green-900 bg-green-600": ''}`} onClick={()=>this.props.handleCompleteTask(task._id)} strokeWidth={0.4} fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path strokeLinecap="round" strokeLinejoin="round" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                    </svg>    
                                                    <span onClick={(e)=>this.onOpenTaskModal(e, task)} style={{lineHeight: '1.5rem'}} className={`${task.completed? "line-through": ''}`}>{task.task_name}</span>
                                                </div>
                                                <svg onClick={()=>this.props.remove(task.id)} xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 absolute right-1 hover:text-red-300" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                </svg>
                                                <hr />
                                            </li> 
                                        )
                                    }
                                    </ul>
                                    <ul className={`flex ${view === 'board' ? 'flex-row flex-wrap' : 'flex-col'}  w-full`}>
                                        {
                                            view === 'board'
                                                ? sections.map((section, index) =>
                                                    <Board handleCompleteTask={this.props.handleCompleteTask} removeSection={this.removeSection} remove={(id)=>this.removeTask(id)} theme={theme} section={section} tasks={tasks} sectionId={'sectionId'} key={index} />
                                                )
                                                : sections.map((section, index) =>
                                                    <List handleCompleteTask={this.props.handleCompleteTask} removeSection={this.removeSection} remove={(id)=>this.removeTask(id)} theme={theme} section={section} tasks={tasks} sectionId={'sectionId'} key={index} />
                                                )
                                        }
                                    </ul>
                                    {
                                        sectionBlur
                                        ?   <div className="w-full text-center py-2 hover:text-red-500 cursor-pointer" onClick={this.addSection}>
                                                Add Section
                                                <hr />
                                            </div>
                                        :   <div className="w-72" style={{margin: 'auto'}}>
                                                <input type="text" className={`w-full mb-4 rounded shadow${theme==="light"?"bg-transparent text-black" : "bg-white text-black" }`}/>
                                                <div className="flex justify-between">
                                                    <button className={`px-4 py-2 rounded shadow ${theme==="light" ?"text-white bg-gray-800" : "bg-white text-black" }`} >Create Task</button>
                                                    <button className={`px-4 py-2 rounded shadow ${theme==="light" ?"text-white bg-gray-800" : "bg-white text-black" }`} onClick={()=>this.setState({...this.state, sectionBlur: true})}>Cancel</button>
                                                </div>
                                            </div>

                                    }
                                    
                                </>
                                :   <>
                                        <NoTask />
                                        {
                                            sectionBlur
                                            ?   <div className="w-full text-center py-2 hover:text-red-500 cursor-pointer" onClick={this.addSection}>
                                                    Add Section
                                                    <hr />
                                                </div>
                                            :   <div className="w-72" style={{margin: 'auto'}}>
                                                    <input value={this.state.newSection} onChange={this.setNewSection} placeholder="section name" type="text" className={`w-full mb-4 rounded shadow${theme==="light"?"bg-transparent text-black" : "bg-white text-black" }`} style={{border: "2px solid gray"}}/>
                                                    <div className="flex justify-between">
                                                        <button onClick={console.log} className={`px-4 py-2 rounded shadow ${theme==="light" ?"text-white bg-gray-800" : "bg-white text-black" }`} >Create Section</button>
                                                        <button className={`px-4 py-2 rounded shadow ${theme==="light" ?"text-white bg-gray-800" : "bg-white text-black" }`} onClick={()=>this.setState({...this.state, sectionBlur: true, newSection: ''})}>Cancel</button>
                                                    </div>
                                                </div>

                                        }
                                    </>
                        }
                    </div>
                </div>
            </>
        )
    }
}

export default TaskView
