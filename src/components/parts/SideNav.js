import axios from 'axios';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './accordian.css';
import { withRouter } from "react-router-dom";
import Modal from 'react-responsive-modal';

class SideNav extends Component {
	constructor(props) {
		super(props)

		this.state = {
			projects: [],
			labels: [],
			projectModal: false,
			labelModal: false,
			newProjectName: '',
			newLabelName: '',
		}
	}
	addLabel = () =>{
		this.setState({
			...this.state,
			labelModal: true
		})
	}
	AddProject = () =>{
		this.setState({
			...this.state,
			projectModal: true
		})
	}
	static getDerivedStateFromProps(nextProps, prevState){
		return {
			...prevState,
			projects: nextProps.projects,
			labels: nextProps.labels
		}
	}
	createProject = () =>{
		const token = localStorage.getItem('token');
		const username = localStorage.getItem('username');
		const apiUrl = 'https://todoist-clone-back.herokuapp.com/';
		axios({
			method: 'post',
			url: apiUrl+`projects/project`,
			headers: {"token": token},
			body: {
				"username": username,
				"project_name": this.state.newProjectName
			}
		})
		.then(res=>{
			this.setState({
				...this.state,
				labels: res.data
			})
		})
		.finally(()=>this.setState({...this.state, projectModal: false}))
		.catch(err=>console.log(err))
	}
	createLabel = () => {
		const token = localStorage.getItem('token');
		const username = localStorage.getItem('username');
		const apiUrl = 'https://todoist-clone-back.herokuapp.com/';
		axios({
			method: 'post',
			url: apiUrl+`labels/label`,
			headers: {"token": token},
			body: {
				"username": username,
				"label_name": this.state.newLabelName
			}
		})
		.then(res=>{
			this.setState({
				...this.state,
				labels: res.data
			})
		})
		.finally(()=>this.setState({...this.state, labelModal: false}))
		.catch(err=>console.log(err))
	}
	render() {
		const { projects, labels } = this.state;
		const theme = this.props.theme;
		return (
			<div className={`${theme==="light"? "bg-gray-50": "bg-gray-800"} w-1/5 pt-4 float-left`}>
				<div className="ml-6 mr-2">
					<ul className="list-none">
						<li className={`p-1 text-sm ${theme==="light"?"hover:bg-gray-200 focus:bg-gray-100" : "hover:bg-gray-500 text-white" } mb-0.5 rounded`}>
							<Link to="/inbox" className="flex justify-start w-full">
								<svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
									<path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M20 13V6a2 2 0 00-2-2H6a2 2 0 00-2 2v7m16 0v5a2 2 0 01-2 2H6a2 2 0 01-2-2v-5m16 0h-2.586a1 1 0 00-.707.293l-2.414 2.414a1 1 0 01-.707.293h-3.172a1 1 0 01-.707-.293l-2.414-2.414A1 1 0 006.586 13H4" />
								</svg>
								<h4 className="ml-2">Inbox</h4>
							</Link>
						</li>
						<li className={`flex justify-start p-1 text-sm ${theme==="light"?"hover:bg-gray-200 focus:bg-gray-100" : "hover:bg-gray-500 text-white" } mb-0.5 rounded`}>
							<Link to="/today" className="flex justify-start w-full">
								<svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
									<path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M8 7V3m8 4V3m-9 8h10M5 21h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z" />
								</svg>
								<h4 className="ml-2">Today</h4>
							</Link>
						</li>
					</ul>
					<ul className="mt-4">
						<li className="list-none p-0.5 mt-1 overflow-hidden rounded-sm">
							<div className={`overflow-hidden ${theme==="light"? "text-black": "text-white"} w-full`}>
								<input type="checkbox" id="project-accordian" className="absolute opacity-0 -z-10" />
								<label className="text-sm tab-label flex justify-between font-semibold cursor-pointer" for="project-accordian">Projects</label>
								<div className="tab-content">
									<ul>
										{
											projects.map(project =>
												<li key={project.id} onClick={()=>this.props.history.push(`/projects/${project.project_name}`)} className="text-sm mt-1 flex justify-start cursor-pointer"><div className="self-center h-2 w-2 mr-2 bg-gray-200" style={{border: "1px solid gray", borderRadius: "0.5rem"}}></div>{project.project_name}</li>
											)
										}
										<li onClick={this.AddProject} className={`text-center p-2 cursor-pointer rounded shadow w-32 ${theme==="light"? "bg-blue-200" : "bg-gray-100 text-black"}`}>Add Project</li>
										<Modal open={this.state.projectModal} onClose={()=>this.setState({...this.state, projectModal: false})}>
											<div className="flex flex-col p-4 m-4">
												<h1 className="p-2 font-lg">Project Name</h1>
												<input onChange={(e)=>this.setState({...this.state, newProjectName: e.target.value})} className="shadow rounded" style={{border: "1px solid black"}} type="text"/>
												<button onClick={this.createProject} className="bg-gray-300 shadow rounded p-2 m-2" style={{border: "1px solid black"}}>Add</button>
											</div>
										</Modal>
									</ul>
								</div>
							</div>
						</li>
						<li className="list-none p-0.5 mt-1 overflow-hidden rounded-sm">
							<div className={`overflow-hidden ${theme==="light"? "text-black": "text-white"} w-full`}>
								<input type="checkbox" id="label-accordian" className="absolute opacity-0 -z-10" />
								<label className="text-sm tab-label flex justify-between font-semibold cursor-pointer" for="label-accordian">Labels</label>
								<div className="tab-content">
									<ul>
										{
											labels.map(label =>
												<li key={label._id} onClick={()=>this.props.history.push(`/labels/${label.label_name}`)} className="text-sm mt-1 flex justify-start cursor-pointer">
													<svg width="24" height="24" viewBox="0 0 24 24"><path fill={`${label.label_color}`} fullRule="nonzero" d="M5.914 11.086l4.5-4.5A2 2 0 0 1 11.828 6H16a2 2 0 0 1 2 2v4.172a2 2 0 0 1-.586 1.414l-4.5 4.5a2 2 0 0 1-2.828 0l-4.172-4.172a2 2 0 0 1 0-2.828zM14 11a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"></path></svg>
													{label.label_name}
												</li>
											)
										}
										<li onClick={this.addLabel} className={`text-center p-2 cursor-pointer rounded shadow w-32 ${theme==="light"? "bg-blue-200" : "bg-gray-100 text-black"}`}>Add Label</li>
										<Modal open={this.state.labelModal} onClose={()=>this.setState({...this.state, labelModal: false})}>
											<div className="flex flex-col p-4 m-4">
												<h1 className="p-2 font-lg">Label Name</h1>
												<input onChange={(e)=>this.setState({...this.state, newLabelName: e.target.value})}  className="shadow rounded" style={{border: "1px solid black"}} type="text"/>
												<button onClick={this.createLabel} className="bg-gray-300 shadow rounded p-2 m-2" style={{border: "1px solid black"}}>Add</button>
											</div>
										</Modal>
									</ul>
								</div>
							</div>
						</li>
					</ul>
					<>

					</>
				</div>
			</div>
		)
	}
}

export default withRouter(SideNav)
