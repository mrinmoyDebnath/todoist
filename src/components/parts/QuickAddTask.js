import axios from 'axios';
import React from 'react'
import { Modal } from 'react-responsive-modal';
import 'react-responsive-modal/styles.css';
import './Modal.css';

class QuickAddTask extends React.Component {
    constructor(props) {
        super(props)
    
        this.state = {
            newTask: '',
            labelName: '',
            projectName: '',
            task_due_date: '',
            sectionName: '',
            sections: [],
            labels: [],
            projects: [],
            sections: []
        }
    }
    componentDidMount(){
        this.getProjects();
        this.getSections();
        this.getLabels();
    }
    addNewTask = (e) => {
        if(e.key==='Enter') console.log('enter')
        this.setState({
            newTask: e.target.value
        })
    }
    getProjects = () =>{
        const token = localStorage.getItem('token');
        const username = localStorage.getItem('username');
        const apiUrl = 'https://todoist-clone-back.herokuapp.com/';
        axios({
          method: 'get',
          url: apiUrl+`projects/${username}/projects`,
          headers: {"token": token},
                // body
        })
        .then(res=>{
          this.setState({
            ...this.state,
            projects: res.data
          })
        })
        .catch(err=>console.error(err))
      }
      getSections = () =>{
        const token = localStorage.getItem('token');
        const username = localStorage.getItem('username');
        const apiUrl = 'https://todoist-clone-back.herokuapp.com/';
        axios({
          method: 'get',
          url: apiUrl+`sections/${username}`,
          headers: {"token": token},
        })
        .then(res=>{
          this.setState({
            ...this.state,
            sections: res.data
          })
        })
      }
      
      getLabels = () =>{
        const token = localStorage.getItem('token');
        const username = localStorage.getItem('username');
        const apiUrl = 'https://todoist-clone-back.herokuapp.com/';
        axios({
          method: 'get',
          url: apiUrl+`labels/${username}`,
          headers: {"token": token},
        })
        .then(res=>{
          this.setState({
            ...this.state,
            labels: res.data
          })
        })
      }
    confirmAddTask = e => {
        const { newTask, labelName, projectName, task_due_date, sectionName } = this.state;
        const token = localStorage.getItem('token');
        const username = localStorage.getItem('username');
        const dueDate = new Date(task_due_date).toISOString();
        const apiUrl = 'https://todoist-clone-back.herokuapp.com/';
        fetch(`${apiUrl}tasks/task`,{
            method: "POST",
            headers: {"Content-type": "application/json; charset=UTF-8", "token": token},
            body: JSON.stringify({
                "username": username,
                "task_name": newTask,
                "in_section": true,
                "directly_in_project": true,
                "task_due_date": dueDate,
                "project_id": projectName,
                "section_id": sectionName
            })
        })
        .then(res=>console.log(res.json()))
        .then(res=>this.props.callAllApis())
        .catch(err=>console.log(err));
    }
    render(){
        const { newTask, labelName, projectName, sectionName, sections, labels, projects, task_due_date } = this.state;
        return (
            <>
                <Modal open={this.props.openModal} onClose={this.props.closeModal} 
                    classNames={{
                        overlay: 'customOverlay',
                        modal: 'customModal',
                    }}
                    contentClassName="modalWidth"
                >
                    <div className="flex flex-col p-1" style={{width: 'max-content'}}>
                        <h3 className="text-sm font-bold text-left mb-4">Quick Add Task</h3>
                        <div className="rounded-lg" style={{border:"1px solid #D3D3D3"}}>
                            <input value={newTask} onChange={this.addNewTask} placeholder="e.g. Meeting" type="text" className="mb-2 p-2 focus:outline-none"/>
                            <div className="flex flex-row">
                            
                            
                                <div className="mr-1 flex justify-center flex-row rounded-lg border-gray-200 cursor-pointer hover:bg-indigo-50" style={{border:"1px solid #D3D3D3"}}>
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M7 20l4-16m2 16l4-16M6 9h14M4 15h14" />
                                    </svg>
                                    <span className="text-green-500 text-xs">
                                        <select onChange={e=>this.setState({...this.state, sectionName: e.target.value})} name="section" className="p-1">
                                            {
                                                sections.map((section, index)=>{
                                                    return <option key={index} value={section._id}>{section.section_name}</option>
                                                })
                                            }
                                        </select>
                                    </span>
                                </div>

                                <div className="mr-1 flex justify-center flex-row rounded-lg border-gray-200 cursor-pointer hover:bg-indigo-50" style={{border:"1px solid #D3D3D3"}}> 
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M7 7h.01M7 3h5c.512 0 1.024.195 1.414.586l7 7a2 2 0 010 2.828l-7 7a2 2 0 01-2.828 0l-7-7A1.994 1.994 0 013 12V7a4 4 0 014-4z" />
                                    </svg>
                                    <span className="text-gray-400 text-xs">
                                        <select onChange={e=>this.setState({...this.state, labelName: e.target.value})} name="label" className="p-1">
                                            {
                                                labels.map((label, index)=>{
                                                    return <option key={index} value={label._id}>{label.label_name}</option>
                                                })
                                            }
                                        </select>
                                    </span>
                                </div>
                                <div className="mr-1 flex justify-center flex-row rounded-lg border-gray-200 cursor-pointer hover:bg-indigo-50" style={{border:"1px solid #D3D3D3"}}> 
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 13l-7 7-7-7m14-8l-7 7-7-7" />
                                    </svg>
                                    <span className="text-gray-400 text-xs">
                                        <select onChange={e=>this.setState({...this.state, projectName: e.target.value})} name="label" className="p-1">
                                            {
                                                projects.map((project, index)=>{
                                                    return <option key={index} value={project._id}>{project.project_name}</option>
                                                })
                                            }
                                        </select>
                                    </span>
                                </div>
                                <div className="flex justify-center flex-row rounded-lg border-gray-200 cursor-pointer hover:bg-indigo-50 ml-2" style={{border:"1px solid #D3D3D3"}}> 
                                    <span className="text-gray-400 text-xs p-1"><input value={task_due_date} onChange={(e)=>this.setState({...this.state, task_due_date: e.target.value})} type="date" /></span>
                                </div>
                                </div>
                        </div>
                            <button onClick={this.confirmAddTask} className="w-1/2 text-center mt-5 bg-red-600 px-3.5 py-2 rounded-lg font-bold text-white hover:bg-red-400">Add task</button>
                    </div>
                </Modal>
            </>
        )
    }
}

export default QuickAddTask
