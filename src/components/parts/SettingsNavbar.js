import React from 'react'
import { Link } from 'react-router-dom'

function SettingsNavbar() {
    return (
        <div className="flex w-1/4 text-lg">
            <ul className="w-full">
                <li>
                    <Link  className="w-full flex flex-start py-2 pl-1 hover:bg-white" to='/settings/account'>
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                        </svg>
                        <div className="ml-1">Account</div>
                    </Link>
                </li>
                <li>
                    <Link  className="w-full flex flex-start py-2 pl-1 hover:bg-white" to='/settings/notifications'>
                        <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9" />
                        </svg>
                        <div className="ml-1">Notification</div>
                    </Link>
                </li>
            </ul>
        </div>
    )
}

export default SettingsNavbar
