import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import QuickAddTask from './QuickAddTask';
import '../pages/themeToggle.css'
import { Redirect } from 'react-router-dom';

class DashboardHeader extends Component {
    constructor(props) {
        super(props)

        this.state = {
            profileBlur: true,
            notificationBlur: true,
            openModal : false,
            navigate: false
        }
        this.profileModal = React.createRef();
        this.notificationModal = React.createRef();
    }
    displayProfile = (e) => {
        this.setState({
            ...this.state,
            profileBlur: false
        }, () => {
            this.profileModal.current.focus();
        })
    }
    hideProfile = (e) => {
        // console.log(this.profileModal.current)
        this.setState({
            ...this.state,
            profileBlur: true
        })
    }
    displayNotification = (e) => {
        this.setState({
            ...this.state,
            notificationBlur: false
        }, () => {
            this.notificationModal.current.focus();
        })
    }
    hideNotification = (e) => {
        // console.log(this.NotificationModal.current)
        this.setState({
            ...this.state,
            notificationBlur: true
        })
    }


    onClickButton = e =>{
        e.preventDefault()
        this.setState({openModal : true})
    }

    onCloseModal = ()=>{
        this.setState({openModal : false})
    }

    logout = () => {
        localStorage.clear();
        this.setState({navigate : true});
    
    }

    render() {
        const { profileBlur, notificationBlur} = this.state;
        const { theme, toggleTheme } = this.props;
        const { navigate } = this.state;


        if(navigate) {
            return <Redirect to="/" push={true} />;
        }

        
        return (
            <div className={`${theme==='light'? 'bg-red-500': 'bg-gray-900'} `}>
            <div className={`flex justify-between py-1`} style={{maxWidth: '100rem', margin: 'auto'}}>
                <div className="left-menu px-8 py-1">
                    <ul className="list-none flex justify-start">
                        <li>
                            <svg xmlns="http://www.w3.org/2000/svg" className="cursor-pointer h-6 w-6 text-gray-50" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={1.5} d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                            </svg>
                        </li>
                        <li className="px-1">
                            <input type="search" name="" id="" className={`outline-none w-48 rounded p-1.5 text-xs ${theme==='light'?"hover:text-black focus:text-black text-white bg-red-400 hover:bg-gray-100 focus:bg-gray-100": ""} focus:w-72`} placeholder="Find"/>
                        </li>
                    </ul>
                </div>
                <div className="right-menu px-8 py-1">
                    <ul className="list-none flex justify-end">
                   <QuickAddTask openModal={this.state.openModal} closeModal={this.onCloseModal} callAllApis={this.props.callAllApis}/>
                    
                        <li className="px-1" onClick={this.onClickButton}>
                            <svg xmlns="http://www.w3.org/2000/svg" className={`cursor-pointer h-6 w-6 ${theme==="light"? "text-gray-50": "text-white" }`} fill="none" viewBox="0 0 24 24" stroke="currentColor">
                              <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={1.5} d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                            </svg>
                        </li>
                        <li className="px-1">
                            <div className="price-switcher">
                                <label className="switch">
                                <input type="checkbox" id="price-type-toggle" onClick={toggleTheme}/>
                                <span className="slider round"></span>
                                </label>
                            </div>
                        </li>
                        <li className="px-1"  tabIndex="0" onFocus={e=>this.displayNotification(e)}>
                            <svg xmlns="http://www.w3.org/2000/svg" className="cursor-pointer h-6 w-6 text-gray-50" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={1.5} d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9" />
                            </svg>
                        </li>
                        <li className="px-1" tabIndex="0" onFocus={e=>this.displayProfile(e)}>
                            <svg xmlns="http://www.w3.org/2000/svg" className="cursor-pointer h-6 w-6 text-gray-50" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={1.5} d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                            </svg>
                        </li>
                        {
                            !profileBlur
                            ?
                            <div>
                            <div tabIndex="0" ref={this.profileModal} onBlur={this.hideProfile} className="w-48 flex flex-col absolute right-2 top-7 bg-gray-50 rounded p-2 pl-2 text-gray-700 shadow">
                                <div className="mb-2 text-sm"><h4>{localStorage.username}</h4></div>
                                <div className="mb-2 text-sm flex hover:bg-gray-100 cursor-pointer">
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z" />
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
                                    </svg>
                                    <h4 className="ml-4"><Link onMouseDown={(e) => e.preventDefault()} to='/settings/account'>Settings</Link></h4>
                                </div>
                                <div className="mb-2 text-sm flex hover:bg-gray-100 cursor-pointer" onClick={this.logout}>
                                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1" />
                                    </svg>
                                    <h4 className="ml-4"><Link onMouseDown={(e) => e.preventDefault()} to="/">Logout</Link></h4>
                                </div>
                            </div>
                            </div>
                            : null
                        }
                        {
                            !notificationBlur
                            ?
                            <div className="relative">
                            <div tabIndex="0" ref={this.notificationModal} onBlur={this.hideNotification} className="w-48 flex flex-col absolute right-2 top-7 bg-gray-50 rounded p-2 pl-2 text-gray-700 shadow">
                                <div className="mb-2 text-sm flex hover:bg-gray-100 cursor-pointer">
                                    <h4 className="ml-4"><Link onMouseDown={(e) => e.preventDefault()} href='/settings/notifications'>Notifications</Link></h4>
                                </div>
                                <div className="mb-2 text-sm flex hover:bg-gray-100 cursor-pointer" onClick={()=>console.log('theme clicked')}>
                                    <h4 className="ml-4">Unread</h4>
                                </div>
                            </div>
                            </div>
                            : null
                        }

                    </ul>
                </div>
            </div>
            </div>
        )
    }
}

export default DashboardHeader
