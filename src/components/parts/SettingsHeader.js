import React from 'react'
import { Link } from 'react-router-dom'

function SettingsHeader() {
    return (
        <div className="w-full py-2 shadow bg-white">
            <div className="flex justify-between h-7 text-2xl" style={{margin: 'auto', maxWidth: "60rem"}}>
                
                <div>Settings</div>
                <Link to='/inbox' className="flex flex-end h-7">
                    {/*<span className="mr-0.5">Close</span>*/}
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-7 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </Link>
            </div>
        </div>
    )
}

export default SettingsHeader
