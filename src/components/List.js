import React from 'react';
import { useState } from "react";
import TaskModal from './TaskModal';

let completed = false;
function List(props) {
    const [openModal, setModal] = useState(false)
    const [currentTask, setCurrentTask] = useState(null)
    const onOpenModal = (e,task) =>{
        e.preventDefault()
        setModal(true)
        setCurrentTask(task)
    }

    const onCloseModal = ()=>{
        setModal(false)
    }
    const section = props.section;
    const tasks = props.tasks;
    const theme = props.theme;
    const sectionTasks = tasks.filter(task=>task.section_id===section._id);
    // console.log(tasks)
    return (
        <li className={`my-2 p-2 rounded-xl ${theme==="light"? "bg-gray-100": "bg-gray-600"}`}>

        <TaskModal openModal={openModal} closeModal={onCloseModal} task={currentTask}/>

            <div className="flex items-center justify-center pl-4 pr-12">
                <div className="overflow-hidden text-black w-full">
                    <input type="checkbox" id={`${section._id}`} className="absolute py-4 opacity-0 -z-10 cursor-pointer w-24 "/>
                    <span className={`text-base font-bold ${theme==="light"? "text-gray-700": "text-white"}`}>{section.section_name}</span>
                    <div className="tab-content flex justify-between relative">
                    <svg onClick={()=>props.removeSection(section._id)} xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 absolute -top-6 right-1 hover:text-red-300" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>

                    <ul className={`${theme==="light"? "text-gray-500": "text-white"} my-1 text-sm mt-4 w-full`}>
                        {
                            sectionTasks.map(task=>
                                <li key={task._id} className={`w-full flex justify-between relative`}>
                                    <div className="flex flex-start p-2 mb-1 cursor-pointer" >
                                        <svg xmlns="http://www.w3.org/2000/svg" className={`h-6 w-6 hover:bg-green-300 rounded-full mr-4 ${completed? 'stroke-2': ''}`} onClick={()=>props.handleCompleteTask(task.id)} strokeWidth={0.4} fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>    
                                        <span  onClick={e=>onOpenModal(e, task)} style={{lineHeight: '1.5rem'}}>{task.task_name}</span>
                                    </div>
                                    <svg onClick={()=>props.remove(task._id)} xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 absolute right-1 hover:text-red-300" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M15 12H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                    </svg>
                                    <hr />
                                </li>
                            )
                        }
                        <li className="flex flex-start pl-2 hover:text-red-700">
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 mr-4" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                            </div>
                            <div className=" w-full">Add task</div>
                        </li>
                    </ul>
                    </div>
                    {props.children ? props.children : null}
                </div>
            </div>
        </li>
    )
}

export default List
