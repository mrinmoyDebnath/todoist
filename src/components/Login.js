import React, { Component } from 'react'
import './Signup.css';
import { Link } from 'react-router-dom';

class Login extends Component {

    constructor() {
        super();
        this.state = {
          input: {},
          errors: {}
        };
         
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    // isLogin to check the user is valid or not 

    isLogin = async (input) => {
        try {
            const promise = await fetch('https://todoist-clone-back.herokuapp.com/users/login', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(input)
            })
    
            const result = await promise.json();
            console.log(result);
            localStorage.setItem("token", result.token);
            localStorage.setItem("username", result.user_data.username)
            this.props.history.push('./inbox')
    
        } catch(error){
            console.log(error);
        }
    
        
    }

    // Getting value from login form

    handleChange(event) {
        let input = this.state.input;
        input[event.target.name] = event.target.value;
        
        this.setState({
          input
        });
    }

    // Handle login data on submit

    handleSubmit(event) {
        event.preventDefault();
        if(this.validate()){
      
            let input = {};
            input["username"] = "";
            input["password"] = "";
            this.setState({input:input});


            return this.isLogin(this.state.input);
              
        }
      }

      // To validate a user details fills data in correct format

      validate(){
        let input = this.state.input;
        let errors = {};
        let isValid = true;
     
        if (!input["username"]) {
          isValid = false;
          errors["username"] = "Please enter your username.";
        }
    
        if (typeof input["username"] !== "undefined") {
          const re = /^[a-zA-Z]+([_]?[a-zA-Z0-9])*$/;
          if(input["username"].length < 6 || !re.test(input["username"])){
              isValid = false;
              errors["username"] = "Please enter valid username.";
          }
        }
    
    
        if (!input["password"]) {
          isValid = false;
          errors["password"] = "Please enter your password.";
        }
    
    
        if (typeof input["password"] !== "undefined") {
          if(input["password"].length < 6){
              isValid = false;
              errors["password"] = "Please add at least 6 character.";
          }
        }
    
    
        this.setState({
          errors: errors
        });
    
        return isValid;
    }

    render() {
        return (
            <main className="signup_page">

                <div className="signup_page_frame">
                    <div className="signup_page_logo">
                        <div aria-label="Todoist Homepage">
                            <img src="https://d3ptyyxy2at9ui.cloudfront.net/logo-e7e40b.svg" alt=""/>
                        </div>
                    </div>

                    <div className="signup_page_content">
                        <div id="signup" className="signup_form">
                            <div class="signup_items">
                                <div class="process_one">

                                    <h1>Log in</h1>

                                    <div className="social_button button_google">
                                        <img width="16" height="16" src="https://d3ptyyxy2at9ui.cloudfront.net/google-41de20.svg" alt=""/>
                                        Continue with Google
                                    </div>
                                    <div className="social_button button_facebook">
                                        <img width="16" height="16" src="https://d3ptyyxy2at9ui.cloudfront.net/facebook-fadd25.svg" alt=""/>
                                        Continue with Facebook
                                    </div>
                                    <div className="social_button button_apple">
                                        <img width="16" height="16" src="https://d3ptyyxy2at9ui.cloudfront.net/apple-728ddf.svg" alt=""/>
                                        Continue with Apple
                                    </div>

                                    <div className="seperator">
                                        <div className="middle_seperator">
                                            <h2><span>OR</span></h2>
                                        </div>
                                    </div>

                                    <form class="email-form" onSubmit={this.handleSubmit}>
                                        <div className="field">
                                            <label className="label" htmlFor="username">Username</label>
                                            <input type="text" name="username" value={this.state.input.username} onChange={this.handleChange} id="username"/>
                                            <div className="isa_error">{this.state.errors.username}</div>
                                        </div>

                                        <div className="field">
                                            <label className="label" htmlFor="password">Password</label>
                                            <div className="relative w1 pr-9">
                                                <input type="password" name="password" value={this.state.input.password} onChange={this.handleChange} id="password" />
                                                <div className="isa_error">{this.state.errors.password}</div>
                                            </div>
                                        </div>

                                        <button className="submit_button submit_button_red">
                                            <span>Log in</span>
                                        </button>
                                    </form>
 
                                    <label className="stay_login_label">
                                        <input type="checkbox"/> Keep me logged in
                                    </label>

                                    <div class="field">
                                        <span>Forgot your password?</span>
                                    </div>
                                    <div className="login-seperator"></div>

                                    <div className="mt-5 text-center text-xs">
                                        <div className="pb-6">
                                            <p>Don't have an account? <Link to="/signup" className="text-red-500">Sign up</Link></p>
                                            <p className="mb-0 mt-2 text-gray-600">Todoist Support</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </main>
        )
    }
}




export default Login;