import React, { Component } from 'react';
import './index.css';
import './App.css';
import DashboardHeader from './components/parts/DashboardHeader';
import SideNav from './components/parts/SideNav';
import { Route , Switch, useRouteMatch} from 'react-router-dom';
import Today from './components/pages/Today';
import Inbox from './components/pages/Inbox';
import Header from './components/Header';
import Upcoming from './components/pages/Upcoming';
import Signup from './components/Signup';
import Login from './components/Login';
import Settings from './components/pages/Settings';
import Homepage from './components/Homepage';
import ProjectView from './components/pages/ProjectView';
import axios from 'axios';
import LabelView from './components/pages/LabelView';

class App extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      theme: '',
      tasks: [],
      projects: [],
      labels: [],
      sections: [],
      currentProject: null
    }
  }

  getProjects = () =>{
    const token = localStorage.getItem('token');
    const username = localStorage.getItem('username');
    const apiUrl = 'https://todoist-clone-back.herokuapp.com/';
    axios({
      method: 'get',
      url: apiUrl+`projects/${username}/projects`,
      headers: {"token": token},
            // body
    })
    .then(res=>{
      this.setState({
        ...this.state,
        projects: res.data
      })
    })
    .catch(err=>console.error(err))
  }
  getSections = () =>{
    const token = localStorage.getItem('token');
    const username = localStorage.getItem('username');
    const apiUrl = 'https://todoist-clone-back.herokuapp.com/';
    axios({
      method: 'get',
      url: apiUrl+`sections/${username}`,
      headers: {"token": token},
    })
    .then(res=>{
      this.setState({
        ...this.state,
        sections: res.data
      })
    })
  }
  getTasks = () =>{
    const token = localStorage.getItem('token');
    const username = localStorage.getItem('username');
    const apiUrl = 'https://todoist-clone-back.herokuapp.com/';
    axios({
      method: 'get',
      url: apiUrl+`tasks/${username}`,
      headers: {"token": token},
    })
    .then(res=>{
      this.setState({
        ...this.state,
        tasks: res.data
      })
    })
  }
  getLabels = () =>{
    const token = localStorage.getItem('token');
    const username = localStorage.getItem('username');
    const apiUrl = 'https://todoist-clone-back.herokuapp.com/';
    axios({
      method: 'get',
      url: apiUrl+`labels/${username}`,
      headers: {"token": token},
    })
    .then(res=>{
      this.setState({
        ...this.state,
        labels: res.data
      })
    })
  }
  callAllApis = () => {
    
    this.getLabels();
    this.getProjects();
    this.getTasks();
    this.getSections();
  }
  componentDidMount(){
    const theme = localStorage.getItem('theme');
    if(!theme){
      this.setState({
        theme: 'light'
      })
      localStorage.setItem('theme', 'light')
    }
    else
    this.setState({
      theme: theme,
    })
    if(localStorage.getItem('token'))
      this.callAllApis();
  }
  toggleTheme = () =>{
    this.setState(prevState=>({
        ...this.state,
        theme: prevState.theme === 'light'? 'dark' : 'light'
    }))
  }
  render() {
    const projects = this.state.projects;
    return (
      <div style={{margin: 'auto'}}>
        <Switch>
          <Route path="/login" component={Login}/>
          <Route path="/signup" component={Signup}/>
          <Route exact path="/">
            <div className="main">
              <Homepage />        
            </div>
          </Route>
          <Route path="/inbox">
            <DashboardHeader callAllApis={this.callAllApis} theme={this.state.theme} getUser={this.getUser} toggleTheme={this.toggleTheme}/>
            <div className="flex justify-start" style={{maxWidth: '100rem', margin: 'auto', minHeight:"100vh"}}>
              <SideNav projects={projects} labels={this.state.labels} theme={this.state.theme}/>
              <Inbox projects={this.state.projects} labels={this.state.labels} sections={this.state.sections} tasks={this.state.tasks} theme={this.state.theme}/>
            </div>
          </Route>
          <Route path="/today">
            <DashboardHeader callAllApis={this.callAllApis} theme={this.state.theme} toggleTheme={this.toggleTheme}/>
            <div className="flex justify-start" style={{maxWidth: '100rem', margin: 'auto', minHeight:"100vh"}}>
              <SideNav projects={projects} labels={this.state.labels} theme={this.state.theme}/>
              <Today projects={this.state.projects} labels={this.state.labels} sections={this.state.sections} tasks={this.state.tasks} theme={this.state.theme}/>
            </div>
          </Route>
          <Route path="/upcoming">
            <DashboardHeader callAllApis={this.callAllApis} theme={this.state.theme} toggleTheme={this.toggleTheme}/>
            <div className="flex justify-start" style={{maxWidth: '100rem', margin: 'auto', minHeight:"100vh"}}>
              <SideNav projects={projects} labels={this.state.labels} theme={this.state.theme}/>
              <Upcoming theme={this.state.theme} sections={this.state.sections}/>
            </div>
          </Route>
          <Route path="/projects/:projectName">
            <DashboardHeader callAllApis={this.callAllApis} theme={this.state.theme} toggleTheme={this.toggleTheme}/>
            <div className="flex justify-start" style={{maxWidth: '100rem', margin: 'auto', minHeight:"100vh"}}>
              <SideNav projects={projects} labels={this.state.labels} theme={this.state.theme}/>
              <ProjectView projects={projects} sections={this.state.sections} labels={this.state.labels} theme={this.state.theme}/>
            </div>
          </Route>
          <Route path="/labels/:labelName">
            <DashboardHeader callAllApis={this.callAllApis} theme={this.state.theme} toggleTheme={this.toggleTheme}/>
            <div className="flex justify-start" style={{maxWidth: '100rem', margin: 'auto', minHeight:"100vh"}}>
              <SideNav projects={projects} labels={this.state.labels} theme={this.state.theme}/>
              <LabelView projects={projects} sections={this.state.sections} labels={this.state.labels} theme={this.state.theme}/>
            </div>
          </Route>
          <Route path="/settings" component={Settings}/>
        </Switch>
      </div>
  
    )
  }
}

export default App;
